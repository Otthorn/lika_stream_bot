# Lika IRC bot for the supreme censorship of banned words

Ceci est un petit bot IRC simpliste, forké de la libraries
[pyirc](https://gitlab.crans.org/esum/pyirc/) et utilisant la lib
[better_profanity](https://github.com/snguyenthanh/better_profanity) pour
mettre en place un simple bot qui copie dans un fichier texte uniquement les
messages considérer par le filtre comme non-choquants.

## Installation

Premièrement, clonnez le repo :
```
git clone https://gitlab.crans.org/otthorn/lika_stream_bot.git
```

Puis installez les dépendances grâce à la pip :
```
pip install -r requirements.txt
```

## Utilisation

Premièrement, modifiez le fichier de configuration `config.yml` à votre
convenance.
Puis :

```
python3 bot.py
```

Vous pouvez run ça dans un `screen|tmux` pour éviter que d'avoir à garder un
shell ouvert.

## Liste des mots bannis

Le fichier `swearwords.txt` est juste un exemple pour l'instant.

## Comment l'utiliser dans OBS

Un faut créer une nouvelle source `text`, selectionner le fichier `output.txt` et
utiliser les options suivantes :

- [x] Read from file
- [x] Enable Antialiasing
- [x] Chat log mode

- Chat log lines: 32
- Custom text width: 80
- [x] Word Wrap

Cette configuration est donné à titre d'exemple.
