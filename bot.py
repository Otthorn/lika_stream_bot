import argparse
import logging
import typing

from dataclasses import dataclass
from time import strftime

import yaml

from irc import IRCClient
from better_profanity import profanity


class ProfanityBot(IRCClient):
    def __init__(self, server, nickname):
        super().__init__(server, nickname)

    def on_privmsg(self, channel, message, target, tags):
        
        sender = target.split("!")[0]
        current_time = strftime('%H:%M')

        if not profanity.contains_profanity(message):
            logging.info(f"Valid message by {sender}.")
            logging.debug(f"{sender}: {message}")
            logfile.write(f"{current_time} {sender}: {message}\n")
            logfile.flush()
        else:
            logging.info(f"Skipping message by {sender}. Reason: profanity deteced.")
            logging.debug(f"{sender}: {message}")

    def on_welcome(self, nickname, reply, target, tags):
        logging.debug(f"Joining IRC room: {config.irc_room}")
        self.join(config.irc_room)

@dataclass
class Config:
    irc_server: str
    irc_username: str
    irc_room: str
    swear_file: str

# Import configuration
parser = argparse.ArgumentParser()
parser.add_argument("-c", "--config", default="config.yml")
parser.add_argument("--log-level", default="INFO")
parser.add_argument("--output-file", default="output.txt")

args = parser.parse_args() 

with open(args.config) as f:
    config_file = yaml.safe_load(f)

config = Config(**config_file)

FORMAT = "%(asctime)s [%(levelname)s] %(message)s"
# Log config
if args.log_level == "DEBUG":
    logging.basicConfig(format=FORMAT, level=logging.DEBUG)
elif args.log_level == "INFO":
    logging.basicConfig(format=FORMAT, level=logging.INFO)

logging.info("Loading configuration")
logging.info(f"Log level: {args.log_level}")
logging.debug(f"Current config: {config}")
logging.debug(f"Ouptut file: {args.output_file}")

# Profanity configuration
logging.info("Loading banned words")
profanity.load_censor_words_from_file(config.swear_file)

# IRC bot configuration
logging.info("Configuring IRC bot")
profanity_bot = ProfanityBot(config.irc_server, config.irc_username)
with open(args.output_file, 'a') as logfile:
    logging.info("Starting IRC bot")
    profanity_bot.start()
